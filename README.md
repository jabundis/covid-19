# README #


### Finalidad de este Repositorio ###

* LLevar acabo medidas preventivas altruistas con base tecnológica en apoyo a personal médico y sociedad en general
* Versión 0.0.1

### ¿Cómo lo instalo? ###

* Instala el programa: git
* Crea un directorio dónde almacenar el contenido de este repositorio
* Entra en dicho directorio con tu interpretador de comandos (bash en linux y mac, ms-dos en windows)
* Clónalo mediante: git clone https://bitbucket.org/jabundis/covid-19
* Les recomiendo esta [página](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet) con un cheatsheet para comandos básicos de git.


### Contribution guidelines vendrán cuando empezemos a programar ###

* Writing tests
* Code review
* Other guidelines

### ¿Contácto? ###

* Jesus Abundis: jesus.abundis@tum.de
